"""
Data module
***********
"""
from .data import (Data, el_nino, tahiti, mascaret, marthe)

__all__ = ['Data', 'el_nino', 'tahiti', 'mascaret', 'marthe']
