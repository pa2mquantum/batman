"""
Mascaret API module
*******************
"""
from .mascaret_api import MascaretApi
from .print_and_plot import (histogram, print_statistics, plot_opt, plot_storage, plot_opt_time, plot_pdf)

__all__ = ["MascaretApi"]
